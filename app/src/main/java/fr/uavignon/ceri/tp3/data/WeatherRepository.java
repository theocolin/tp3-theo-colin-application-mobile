package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CityDao;
import fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase.databaseWriteExecutor;

public class WeatherRepository {

    private static final String TAG = WeatherRepository.class.getSimpleName();

    private LiveData<List<City>> allCities;
    private MutableLiveData<City> selectedCity;

    private volatile MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Boolean> villeEtCodeIncorrect;
    private volatile MutableLiveData<Throwable> webServiceThrowable;
    private Integer nbCities;

    public OWMInterface api;

    private CityDao cityDao;


    private static volatile WeatherRepository INSTANCE;

    public synchronized static WeatherRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new WeatherRepository(application);
        }

        return INSTANCE;
    }

    public WeatherRepository(Application application) {
        Retrofit retrofit =
                new Retrofit.Builder()
                        .baseUrl("https://api.openweathermap.org/data/2.5/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(OWMInterface.class);

        WeatherRoomDatabase db = WeatherRoomDatabase.getDatabase(application);
        cityDao = db.cityDao();
        allCities = cityDao.getAllCities();
        selectedCity = new MutableLiveData<>();
        webServiceThrowable = new MutableLiveData<>();
        isLoading = new MutableLiveData<>(false);
        villeEtCodeIncorrect = new MutableLiveData<>();
    }

    public LiveData<List<City>> getAllCities() {
        return allCities;
    }

    public MutableLiveData<City> getSelectedCity() {
        return selectedCity;
    }

    public MutableLiveData<Boolean> getIsLoading() {return isLoading;}

    public MutableLiveData<Boolean> getVilleEtCodeIncorrect() {return villeEtCodeIncorrect;}

    public MutableLiveData<Throwable> getWebServiceThrowable() {return webServiceThrowable;}


    public long insertCity(City newCity) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return cityDao.insert(newCity);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(newCity);
        return res;
    }

    public int updateCity(City city) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return cityDao.update(city);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(city);
        return res;
    }

    public void deleteCity(long id) {
        databaseWriteExecutor.execute(() -> {
            cityDao.deleteCity(id);
        });
    }

    public void getCity(long id)  {
        Future<City> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return cityDao.getCityById(id);
        });
        try {
            selectedCity.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void loadWeatherCity(City city) {
        isLoading.postValue(true);
        villeEtCodeIncorrect.postValue(false);


        api.getWeather(city.getName()+",,"+city.getCountryCode(), "7137af9bcc47e077bff792af4a32fa07").enqueue(
                new Callback<WeatherResponse>() {
                    @Override
                    public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                        if(response.isSuccessful()) {
                            if(nbCities != null) {
                                nbCities--;
                                isLoading.postValue(true);
                                Log.e("ONRESPONSE", "Nombre d'API restant : " + nbCities.toString());

                            }
                            WeatherResult.transferInfo(response.body(), city);
                            updateCity(city);
                        }
                        else {
                            Log.d("ONRESPONSE", "Erreur : Ville ou pays communiquer innexistant");
                            villeEtCodeIncorrect.postValue(true);
                        }
                        if(nbCities == null || nbCities == 0) {
                            isLoading.postValue(false);
                        }
                    }

                    @Override
                    public void onFailure(Call<WeatherResponse> call, Throwable t) {
                        Log.d("ONFAILURE", "Web service erreur");

                        webServiceThrowable.postValue(t);
                        isLoading.postValue(false);

                    }
                }
        );
    }

    public void loadWeatherAllCities() {
        List<City> allCitiesToLoad = cityDao.getSynchrAllCities();

        nbCities = allCitiesToLoad.size();

        Log.d("testMAJ", isLoading.getValue().toString());

        for (City city : allCitiesToLoad) {
            loadWeatherCity(city);
        }
    }

}
