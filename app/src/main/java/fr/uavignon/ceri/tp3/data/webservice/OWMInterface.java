package fr.uavignon.ceri.tp3.data.webservice;

import fr.uavignon.ceri.tp3.data.WeatherResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OWMInterface {
    @Headers("Accept: application/geo+json")
    @GET("weather")
    Call<WeatherResponse> getWeather(@Query("q") String nomEtCodeVille,
                                     @Query("APIkey") String apiKey);
}
