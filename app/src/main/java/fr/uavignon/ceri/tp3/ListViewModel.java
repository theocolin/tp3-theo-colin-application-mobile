package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.WeatherRepository;

public class ListViewModel extends AndroidViewModel {
    private WeatherRepository repository;
    private LiveData<List<City>> allCities;
    private volatile MutableLiveData<Boolean> isLoading;
    private volatile MutableLiveData<Throwable> webServiceThrowable;

    public ListViewModel (Application application) {
        super(application);
        repository = WeatherRepository.get(application);
        allCities = repository.getAllCities();
        isLoading = new MutableLiveData<>();
        webServiceThrowable = new MutableLiveData<>();
    }

    public void setIsLoading() {
        isLoading = repository.getIsLoading();
    }
    public void setWebServiceThrowable() {
        webServiceThrowable = repository.getWebServiceThrowable();
    }

    MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public void updateAllCities() {
        isLoading.setValue(true);
        Thread t = new Thread() {
            public void run() {
                repository.loadWeatherAllCities();

            }
        };
        t.start();
    }

    MutableLiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }

    LiveData<List<City>> getAllCities() {
        return allCities;
    }

    public void deleteCity(long id) {
        repository.deleteCity(id);
    }


}
